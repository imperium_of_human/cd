Конструкторская документация проекта «Империум Человека»
=========================================================

.. table:: Руководство

    ==================================   =================     
    Руководитель разработки,Основатель   Иван Магрегор
    Ведущий разработчик/мейнтейнер       Денис Рева
    Информационная поддержка             Степан Зотов
    ==================================   =================     

Основная задача, которая ставится перед желающими изменить мир — в какую сторону его менять. Это зовётся планом или же конструкторской документацией. Здесь в деталях описываются все те идеи, вся та система, которая разрабатывается проектом «Империум человека». Присоединяйтесь.


Суммарная информация
=====================

Причины появления проекта
+++++++++++++++++++++++++++

Крах капитализма при полном отсутствии жизнеспособных альтернатив. Очевидная невозможность марксизма отвечать современным организационным вызовам. 

В более широком смысле — организационный кризис конца 20-начала 21 века.

Название проекта «Империум человека» предложил Иван Магрегор по аналогии с игровой и книжной вселенной WarHammer 40k.

О новом поколении разработки.
++++++++++++++++++++++++++++++

Разработка теперь ведётся централизовано через систему контроля версий git.

Ключевое отличие от первого поколения 2014-2020 года — за основу взята тектология А. Богданова, она же организационная наука. Я, Рева Д., произведу значительную её модернизацию и постараюсь как сделать солидное основание в виде теории антропогенеза Б.Поршнева, так и разработать применение тектологии в программировании.

Так же будет список ключевых компонентов.

Внедрение и сроки
++++++++++++++++++

2030 год — готовы все ключевые компоненты. В 2030 году состоятся выборы президента, там и посмотрим. 

Как присоединиться
+++++++++++++++++++

Напишите на электронную почту denis7774@gmail.com или ingwe@mail.imperium.org.ru, либо ещё каким-либо способом свяжитесь с Рева Д.
